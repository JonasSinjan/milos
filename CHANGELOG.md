## v0.1.0 (15/09/2023)

- First release of `pymilos`

## v0.2.0 (16/09/2023)

- First beta of `milos`
- changed directory organization and fix bugs

## v0.3.0 (26/09/2023)

- Back to `pymilos`
- changed directory organization again and fix bugs

## v0.4.0 (20/11/2023)

- Added two new options to initialize the source function and rerun bad inverted pixels
- Started to clean the code (much to do here)