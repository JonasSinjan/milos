import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import subprocess
import time

# here import the pymilos cython code
from milos import milos
#although not necessaty these are the dtype to be passed to C
DTYPE_INT = np.intc
DTYPE_DOUBLE = np.float_

# **************
# TEST INVERSION
# **************

#test data in IDL save format
from scipy.io import readsav
s = readsav('../test_data/test_data.save')
datos = s.data
wave_axis = s.wave_axis
datos = datos[:,:,0:100,0:100]
#datos = np.tile(datos, 4) 

print('shape input',datos.shape) 
# the pipeline has (for the moment being) the data in
# (4, 6, 298, 1176) (pol,wave, y,x)
# This has to be changed to (yx,pol,wave) for C
                                                                                                                                                                                                                 
datos = np.einsum('ijkl->klij',datos)
y,x,p,l = datos.shape
datos = np.reshape(datos,(y*x,p,l))
yx,p,l = datos.shape
print('New shape',datos.shape)                                                                                                                                                                                                                      

#the next input to pymilos is the options
#for the moment no PSF is included 
# and classical estimates are deactivated.
# these will be added in following versions

options = np.zeros((9))#,dtype=DTYPE_INT)
options[0] = len(wave_axis) #NLAMBDA wave axis dimension
options[1] = 15 #MAX_ITER max number of iterations
options[2] = 1 #CLASSICAL_ESTIMATES [0,1] classical estimates ON or OFF
options[3] = 0 #RFS [0,1,2] 0.-> Inversion, 1-> synthesis 0-> RFS
# Only inversion is now available
options[4] = 0 #FWHM(in A)
options[5] = 0 #DELTA(in A)
options[6] = 0 #NPOINTS

print('----------- pmilos ---------')
start = time.process_time()
out = milos.pymilos.pymilos(options,datos,wave_axis)
print(time.process_time() - start)
print(out.shape)
#output has npy*npx 
out = np.reshape(out,(y,x,12))                                                                                                                                                                                        


fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(121)
im1 = ax1.imshow(out[:,:,2], interpolation='None')

divider = make_axes_locatable(ax1)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im1, cax=cax, orientation='vertical')

ax2 = fig.add_subplot(122)
im2 = ax2.imshow(out[:,:,8], interpolation='None')

divider = make_axes_locatable(ax2)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im2, cax=cax, orientation='vertical')

plt.show()

options = np.zeros((9))#,dtype=DTYPE_INT)
options[0] = len(wave_axis) #NLAMBDA wave axis dimension
options[1] = 15 #MAX_ITER max number of iterations
options[2] = 1 #CLASSICAL_ESTIMATES [0,1] classical estimates ON or OFF
options[3] = 0 #RFS [0,1,2] 0.-> Inversion, 1-> synthesis 0-> RFS
# Only inversion is now available
options[4] = 105 #FWHM(in A)
options[5] = 70 #DELTA(in A)
options[6] = options[0] #NPOINTS

print('----------- pmilos ---------')
start = time.process_time()
out = milos.pymilos.pymilos(options,datos,wave_axis)
print(time.process_time() - start)
print(out.shape)
#output has npy*npx 
out = np.reshape(out,(y,x,12))                                                                                                                                                                                        


fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(121)
im1 = ax1.imshow(out[:,:,2], interpolation='None')

divider = make_axes_locatable(ax1)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im1, cax=cax, orientation='vertical')

ax2 = fig.add_subplot(122)
im2 = ax2.imshow(out[:,:,8], interpolation='None')

divider = make_axes_locatable(ax2)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im2, cax=cax, orientation='vertical')

plt.show()
